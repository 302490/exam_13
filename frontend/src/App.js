import React, {Component, Fragment} from 'react';
import {Route, Switch, withRouter} from "react-router-dom";
import {Container} from "reactstrap";
import {connect} from "react-redux";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {logoutUser} from "./store/actions/usersActions";
import MainPage from "./containers/MainPage/MainPage";
import AddPlace from "./containers/AddPlace/AddPlace";
import PlacePage from "./containers/PlacePage/PlacePage";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar
                    user={this.props.user}
                    logout={this.props.logoutUser}
                />
            </header>
            <Container style={{marginTop: '20px'}}>
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/add_place" exact component={AddPlace}/>
                    <Route path="/places/:id" exact component={PlacePage}/>
                </Switch>
            </Container>
        </Fragment>
    );
  }
}


const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

