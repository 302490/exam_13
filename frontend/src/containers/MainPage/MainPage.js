import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllPlaces, removePlace} from "../../store/actions/placesActions";
import {Col} from "reactstrap";
import PlaceItem from "../../components/PlaceItem/PlaceItem";
import config from "../../config";


class MainPage extends Component {

  componentDidMount() {
    this.props.onGetAllPlaces();
  }

  render() {
    let places;

    (this.props.allPlaces) ? (
      places = this.props.allPlaces.map(item => {
        return <Col sm={3} key={item._id}>
          <PlaceItem
            title={item.title}
            imageSrc={config.apiUrl + '/uploads/' + item.titleImage}
            imageAlt={item.titleImage}
            to={'/places/' + item._id}
            rate={item.rateTotal}
            showButton={this.props.user && this.props.user.role === 'admin'}
            onClickRemove={() => this.props.onRemovePlace(item._id)}
          />
        </Col>
      })
    ) : places = null;

    return (
      <div>
        {places}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    allPlaces: state.places.allPlaces
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetAllPlaces: () => dispatch(getAllPlaces()),
    onRemovePlace: id => dispatch(removePlace(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);