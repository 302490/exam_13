import React, {Component} from 'react';
import {connect} from "react-redux";
import {getPlaceItemById} from "../../store/actions/placesActions";
import config from "../../config";
import {Button, CardImg, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import SelectElement from "../../components/UI/Form/SelectElement";
import {addReview, getReviews} from "../../store/actions/reviewsActions";
import ReviewItem from "../../components/ReviewItem/ReviewItem";
import {addImage} from "../../store/actions/imagesActions";
import {getImages} from "../../store/actions/imagesActions";

class PlacePage extends Component {
  state = {
    message: '',
    rateKitchen: '',
    rateCosy: '',
    rateService: '',
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();
    const formData = {
      message: this.state.message,
      rateKitchen: this.state.rateKitchen,
      rateCosy: this.state.rateCosy,
      rateService: this.state.rateService
    };
    if (this.props.user && this.props.placeItem) {
      formData.userId = this.props.user._id;
      formData.placeId = this.props.placeItem._id;
      this.props.onAddReview(formData);
    }
  };

  submitImageFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    formData.append('image', this.state.image);

    if (this.props.user && this.props.placeItem) {
      formData.append('userId', this.props.user._id);
      formData.append('placeId', this.props.placeItem._id);
    }
    this.props.onAddImage(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };


  componentDidMount() {
    this.props.onGetPlaceItemById(this.props.match.params.id);
    this.props.onGetImages(this.props.match.params.id);
    this.props.onGetReviews(this.props.match.params.id);
  };


  render() {
    let placeInfo;
    const place = this.props.placeItem;
    (this.props.placeItem) ? (
      placeInfo = (<div>
        <h1>{place.title}</h1>
        <p>{place.description}</p>
        <CardImg top width="20%" src={config.apiUrl + '/uploads/' + place.titleImage} alt={place.titleImage}/>

        <h2>Gallery</h2>
        <div>
          {this.props.images ? this.props.images.map(image => {
            return <img style={{'width': '200px'}} src={config.apiUrl + '/uploads/' + image.title} alt={image.title}
                        key={image._id}
            />
          }) : null}
        </div>

        <h2>Rating</h2>
        <div>
          <p>Overall: {place.rateTotal}</p>
          <p>Kitchen: {place.rateKitchen}</p>
          <p>Cosy: {place.rateCosy}</p>
          <p>Service: {place.rateService}</p>
        </div>
      </div>)
    ) : placeInfo = null;

    const addReview = <div>
      <h2>Add review</h2>
      <Form onSubmit={this.submitFormHandler}>
        <FormElement
          type="text"
          propertyName="message"
          title="Message"
          value={this.state.message}
          onChange={this.inputChangeHandler}
          required
        />
        <SelectElement
          name="rateKitchen"
          title="Rate Kitchen"
          value={this.state.rateKitchen}
          change={this.inputChangeHandler}
          rates={['1', '2', '3']}  //вынести
        />
        <SelectElement
          name="rateCosy"
          title="Rate Cosy"
          value={this.state.rateCosy}
          change={this.inputChangeHandler}
          rates={['1', '2', '3']}
        />
        <SelectElement
          name="rateService"
          title="Rate Service"
          value={this.state.rateService}
          change={this.inputChangeHandler}
          rates={['1', '2', '3']}
        />
        <FormGroup>
          <Col sm={10}>
            <Button type="submit">Add</Button>
          </Col>
        </FormGroup>
      </Form>
    </div>;


    let reviews;
    (this.props.reviews) ? (
      reviews = this.props.reviews.map(item => {
        return <ReviewItem
          key={item._id}
          date={item.date.slice(0, 24)}
          message={item.message}
          author={item.userId.displayName}
          rateKitchen={item.rateKitchen}
          rateCosy={item.rateCosy}
          rateService={item.rateService}
        />
      })
    ) : (
      reviews = null
    );

    const addImage = <div>
      <h2>Add image</h2>
      <Form onSubmit={this.submitImageFormHandler}>
        <FormElement
          propertyName="image"
          title="Image"
          type="file"
          onChange={this.fileChangeHandler}
          required
        />

        <FormGroup>
          <Col sm={10}>
            <Button type="submit">Add</Button>
          </Col>
        </FormGroup>
      </Form>
    </div>;


    return (
      <div>
        {placeInfo}
        <h2>Reviews</h2>
        {reviews}
        {addReview}
        {addImage}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    placeItem: state.places.placeItem,
    reviews: state.reviews.reviews,
    images: state.images.images
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetPlaceItemById: (id) => dispatch(getPlaceItemById(id)),
    onGetImages: (id) => dispatch(getImages(id)),
    onAddReview: data => dispatch(addReview(data)),
    onGetReviews: id => dispatch(getReviews(id)),
    onAddImage: data => dispatch(addImage(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PlacePage);