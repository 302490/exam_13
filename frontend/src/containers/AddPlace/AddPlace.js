import React, {Component} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {addPlace} from "../../store/actions/placesActions";

class AddPlace extends Component {
  state = {
    title: '',
    description: '',
    titleImage: '',
    userId: this.props.user._id,
    agree: false
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });
    if (this.state.agree) {
      this.props.onSubmit(formData);
    }
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  checkboxChangeHandler = event => {
    this.setState({
      agree: !this.state.agree
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <div>
        <h2>Add new place!</h2>
        <Form onSubmit={this.submitFormHandler}>

          <FormElement
            type="text"
            propertyName="title"
            title="Title"
            value={this.state.title}
            onChange={this.inputChangeHandler}
            required
          />

          <FormElement
            type="text"
            propertyName="description"
            title="Description"
            value={this.state.description}
            onChange={this.inputChangeHandler}
            required
          />

          <FormElement
            propertyName="titleImage"
            title="Photo"
            type="file"
            onChange={this.fileChangeHandler}
            required
          />

          <FormElement
            propertyName="agree"
            title="I agree with terms of using"
            type="checkbox"
            value={this.state.agree}
            onChange={this.checkboxChangeHandler}
            required
          />

          <FormGroup>
            <Col sm={10}>
              <Button type="submit">Add</Button>
            </Col>
          </FormGroup>

        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: data => dispatch(addPlace(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPlace);