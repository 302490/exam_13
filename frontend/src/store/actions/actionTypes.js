export const GET_ALL_PLACES_REQUEST = 'GET_ALL_PLACES_REQUEST';
export const GET_ALL_PLACES_SUCESS = 'GET_ALL_PLACES_SUCESS';
export const GET_ALL_PLACES_FAILURE = 'GET_ALL_PLACES_FAILURE';

export const GET_PLACE_ITEM_REQUEST = 'GET_PLACE_ITEM_REQUEST';
export const GET_PLACE_ITEM_SUCCESS = 'GET_PLACE_ITEM_SUCCESS';
export const GET_PLACE_ITEM_FAILURE = 'GET_PLACE_ITEM_FAILURE';

export const GET_REVIEWS_REQUEST = 'GET_REVIEWS_REQUEST';
export const GET_REVIEWS_SUCCESS = 'GET_REVIEWS_SUCCESS';
export const GET_REVIEWS_FAILURE = 'GET_REVIEWS_FAILURE';

export const GET_IMAGES_REQUEST = 'GET_IMAGES_REQUEST';
export const GET_IMAGES_SUCCESS = 'GET_IMAGES_SUCCESS';
export const GET_IMAGES_FAILURE = 'GET_IMAGES_FAILURE';

export const ADD_REVIEW_REQUEST = 'ADD_REVIEW_REQUEST';
export const ADD_REVIEW_SUCCESS = 'ADD_REVIEW_SUCCESS';
export const ADD_REVIEW_FAILURE = 'ADD_REVIEW_FAILURE';

export const getAllPlacesSuccess = data => ({type: GET_ALL_PLACES_SUCESS, data});
export const getAllPlacesFailure = error => ({type: GET_ALL_PLACES_FAILURE, error});

export const getPlaceItemSuccess = data => ({type: GET_PLACE_ITEM_SUCCESS, data});
export const getPlaceItemFailure = error => ({type: GET_PLACE_ITEM_FAILURE, error});

export const getReviewsSuccess = data => ({type: GET_REVIEWS_SUCCESS, data});
export const getReviewsFailure = error => ({type: GET_REVIEWS_FAILURE, error});

export const getImagesSuccess = data => ({type: GET_IMAGES_SUCCESS, data});
export const getImagesFailure = error => ({type: GET_IMAGES_FAILURE, error});
