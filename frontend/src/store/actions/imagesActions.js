import axios from "../../axios-api";
import {getImagesSuccess} from "../actions/actionTypes";

export const getImages = id => {
  return (dispatch) => {
    return axios.get('/images/' + id).then(response => {
        dispatch(getImagesSuccess(response.data));
      }
    )
  };
};

export const addImage = (data) => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post('/images', data, {headers}).then(response => {
        dispatch(getImages(getState().places.placeItem._id))
      }
    )
  };
};