import {push} from "connected-react-router";
import axios from '../../axios-api';
import {getAllPlacesSuccess, getPlaceItemSuccess} from "./actionTypes";


export const addPlace = (data) => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post('/places', data, {headers}).then(response => {
        dispatch(getAllPlaces());
        dispatch(push('/'));
      }
    )
  };
};

export const getAllPlaces = () => {
  return (dispatch) => {
    return axios.get('/places').then(response => {
        dispatch(getAllPlacesSuccess(response.data));
      }
    )
  };
};

export const getPlaceItemById = id => {
  return (dispatch) => {
    return axios.get('/places/' + id).then(response => {
        dispatch(getPlaceItemSuccess(response.data));
      }
    )
  };
};

export const removePlace = id => {
  return (dispatch) => {
    return axios.delete('/places/' + id).then(response => {
        dispatch(getAllPlaces());
      }
    )
  };
};

