import axios from "../../axios-api";
import {getReviewsSuccess} from "./actionTypes";

export const addReview = (data) => {
  return (dispatch, getState) => {
    const headers = {
      Token: getState().users.user.token
    };
    return axios.post('/reviews', data, {headers}).then(response => {
      dispatch(getReviews(getState().places.placeItem._id));
      }
    )
  };
};

export const getReviews = (id) => {
  return (dispatch) => {
    return axios.get('/reviews/' + id).then(response => {
        dispatch(getReviewsSuccess(response.data));
      }
    )
  };
};