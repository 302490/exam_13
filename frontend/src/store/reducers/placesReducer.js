import {GET_ALL_PLACES_SUCESS, GET_PLACE_ITEM_SUCCESS} from "../actions/actionTypes";

const initialState = {
  allPlaces: null,
  placeItem: null
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_PLACES_SUCESS:
      return {...state, allPlaces: action.data};
    case GET_PLACE_ITEM_SUCCESS:
      return {...state, placeItem: action.data};
    default:
      return state;
  }
};

export default placesReducer;
