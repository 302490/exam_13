import {GET_IMAGES_SUCCESS} from "../actions/actionTypes";

const initialState = {
  images: null
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_IMAGES_SUCCESS:
      return {...state, images: action.data};
    default:
      return state;
  }
};

export default placesReducer;
