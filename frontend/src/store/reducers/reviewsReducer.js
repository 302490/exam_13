import {GET_REVIEWS_SUCCESS} from "../actions/actionTypes";


const initialState = {
  reviews: null
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REVIEWS_SUCCESS:
      return {...state, reviews: action.data};
    default:
      return state;
  }
};

export default placesReducer;
