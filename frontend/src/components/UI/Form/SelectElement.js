import React from 'react';
import {Col, FormGroup, Input, Label} from "reactstrap";

const SelectElement = (props) => {
  return (
    <div>
      <FormGroup row>
        <Label sm={2} for={props.name}>{props.title}</Label>
        <Col sm={10}>
          <Input
            type="select" required
            name={props.name} id={props.name}
            value={props.value}
            onChange={props.change}
          >
            <option value="">Please rate</option>
            {props.rates.map(rate => (
              <option key={rate} value={rate}>{rate}</option>
            ))}
          </Input>
        </Col>
      </FormGroup>
    </div>
  );
};

export default SelectElement;