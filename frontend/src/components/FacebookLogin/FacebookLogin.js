import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "reactstrap";
import {connect} from "react-redux";

import {facebookLogin} from "../../store/actions/usersActions";


class FacebookLogin extends Component {

    facebookLogin = (data) => {
        if (data.error) {
            console.log('something went wrong');
        } else if (!data.name) {
            console.log('you pressed cancel');
        } else {
            this.props.facebookLogin(data);
        }
    };

    render() {
        return (
            <FacebookLoginButton
                appId="431996754015120"
                callback={this.facebookLogin}
                fields="name, email, picture"
                render={renderProps => (
                    <Button onClick={renderProps.onClick} color="primary">
                        Login with Facebook
                    </Button>
                )}
            />

        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        facebookLogin: userData => dispatch(facebookLogin(userData))
    };
};

export default connect(null, mapDispatchToProps)(FacebookLogin);