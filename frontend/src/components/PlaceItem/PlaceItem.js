import React, {Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardSubtitle, CardTitle, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import PropTypes from 'prop-types';

const PlaceItem = props => {
  return (
    <Fragment>
      <NavLink tag={RouterNavLink} to={props.to} exact>
        <Card onClick={props.click} style={{cursor: 'pointer'}}>
          <CardImg top width="100%" src={props.imageSrc} alt={props.imageAlt}/>
          <CardBody>
            <CardTitle>{props.title}</CardTitle>
            <CardSubtitle>Rating: {props.rate}</CardSubtitle>
            <CardSubtitle>{props.photos}</CardSubtitle>
          </CardBody>
        </Card>
      </NavLink>
      {props.showButton ? <Button onClick={props.onClickRemove} style={{'margin': '0 0 50px 17px'}}>Remove</Button> : null}
    </Fragment>
  );
};

PlaceItem.propTypes = {
  imageSrc: PropTypes.string,
  imageAlt: PropTypes.string,
  title: PropTypes.string,
  rate: PropTypes.number,
  photos: PropTypes.string,
  click: PropTypes.func,
  to: PropTypes.string
};


export default PlaceItem;







