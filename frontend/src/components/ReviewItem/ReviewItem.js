import React from 'react';

const ReviewItem = (props) => {
  return (
    <div>
      <p>{props.date}</p>
      <b>{props.author}:  </b>
      <i>{props.message}</i>
      <p>Kitchen rate: {props.rateKitchen}</p>
      <p>Cosy rate: {props.rateCosy}</p>
      <p>Service rate: {props.rateService}</p>
    </div>
  );
};

export default ReviewItem;