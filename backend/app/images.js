const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Image = require('../models/Image');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.single('image')], (req, res) => {
  const imageData = req.body;

  if (req.file) {
    imageData.title = req.file.filename;
  }
  const image = new Image(imageData);

  image.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.get('/', (req, res) => {
  Image.find()
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Image.find({placeId: req.params.id})
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});




module.exports = router;
