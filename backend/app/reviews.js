const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Review = require('../models/Review');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.post('/', auth, (req, res) => {
  const reviewData = req.body;
  reviewData.date = new Date();
  const review = new Review(reviewData);

  review.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.get('/:id', (req, res) => {
  Review.find({placeId: req.params.id})
    .populate('userId')
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

router.get('/', (req, res) => {
  Review.find()
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

module.exports = router;