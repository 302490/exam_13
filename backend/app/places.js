const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Place = require('../models/Place');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.post('/', [auth, upload.single('titleImage')], (req, res) => {
  const placeData = req.body;
  if (!placeData.agree) {
    res.status(400).send('You should agree terms of using')
  }

  if (req.file) {
    placeData.titleImage = req.file.filename;
  }
  delete placeData.agree;
  const place = new Place(placeData);

  place.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.get('/', (req, res) => {
  Place.find()
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Place.findOne({_id: req.params.id})
    .populate('userId')
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
  Place.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500));
});



module.exports = router;
