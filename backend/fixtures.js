const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Place = require('./models/Place');
const Image = require('./models/Image');
const Review = require('./models/Review');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {username: 'u1', password: '1', displayName: 'User One'},
    {username: 'u2', password: '2', displayName: 'User Two'},
    {username: 'admin', password: 'admin', displayName: 'Admin', role: 'admin'}
  );

  const places = await Place.create(
    {
      userId: users[0]._id,
      title: 'Place1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae cum illum incidunt non officia quam, quisquam tempore vel vero.',
      titleImage: '1.jpeg',
      rateKitchen: 4,
      rateService: 5,
      rateCosy: 3,
      rateTotal: 4
    },
    {
      userId: users[0]._id,
      title: 'Place2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae cum illum incidunt non officia quam, quisquam tempore vel vero.',
      titleImage: '2.jpeg',
      rateKitchen: 4,
      rateService: 5,
      rateCosy: 3,
      rateTotal: 4
    },
    {
      userId: users[1]._id,
      title: 'Place3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae cum illum incidunt non officia quam, quisquam tempore vel vero.',
      titleImage: '3.jpg',
      rateKitchen: 5,
      rateService: 5,
      rateCosy: 5,
      rateTotal: 5
    },
    {
      userId: users[1]._id,
      title: 'Place4',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad beatae cum illum incidunt non officia quam, quisquam tempore vel vero.',
      titleImage: '4.jpg',
      rateKitchen: 5,
      rateService: 5,
      rateCosy: 5,
      rateTotal: 5
    }

  );

  const images = await Image.create(
    {userId: users[0]._id, placeId: places[0]._id, title: '1.jpeg'},
    {userId: users[1]._id, placeId: places[0]._id, title: '2.jpeg'},
    {userId: users[0]._id, placeId: places[1]._id, title: '1.jpeg'},
    {userId: users[1]._id, placeId: places[1]._id, title: '2.jpeg'},
    {userId: users[0]._id, placeId: places[2]._id, title: '1.jpeg'},
    {userId: users[1]._id, placeId: places[2]._id, title: '2.jpeg'},
    {userId: users[0]._id, placeId: places[3]._id, title: '1.jpeg'},
    {userId: users[1]._id, placeId: places[3]._id, title: '2.jpeg'}
  );

  const reviews = await Review.create(
    {userId: users[0]._id, placeId: places[0]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'nice place'},
    {userId: users[1]._id, placeId: places[0]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'cool place thank you'},
    {userId: users[0]._id, placeId: places[1]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'nice place'},
    {userId: users[1]._id, placeId: places[1]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'cool place thank you'},
    {userId: users[0]._id, placeId: places[2]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'nice place'},
    {userId: users[1]._id, placeId: places[2]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'cool place thank you'},
    {userId: users[0]._id, placeId: places[3]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'nice place'},
    {userId: users[1]._id, placeId: places[3]._id, date: new Date(), rateKitchen: 3, rateService: 4, rateCosy: 5, message: 'cool place thank you'}
  );

  await connection.close();
};

run().catch(error => {
  console.error('Something went wrong', error);
});
