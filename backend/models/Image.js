const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  placeId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Place'
  },
  title: {
    type: String,
    required: true
  }
});

const Image = mongoose.model('Photo', ImageSchema);

module.exports = Image;
