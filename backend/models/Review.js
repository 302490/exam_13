const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  placeId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Place'
  },
  date: {
    type: String,
    required: true
  },
  rateKitchen: {
    type: Number,
    required: true,
    enum: [1, 2, 3, 4, 5]
  },
  rateService: {
    type: Number,
    required: true,
    enum: [1, 2, 3, 4, 5]
  },
  rateCosy: {
    type: Number,
    required: true,
    enum: [1, 2, 3, 4, 5]
  },
  message: {
    type: String
  }
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;
