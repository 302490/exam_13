const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  titleImage: {
    type: String,
    required: true
  },
  rateKitchen: {
    type: Number,
    required: true,
    enum: [1, 2, 3, 4, 5],
    default: 0

  },
  rateService: {
    type: Number,
    required: true,
    enum: [1, 2, 3, 4, 5],
    default: 0

  },
  rateCosy: {
    type: Number,
    enum: [1, 2, 3, 4, 5],
    default: 0

  },
  rateTotal: {
    type: Number,
    required: true,
    default: 0
  }
});


const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;
